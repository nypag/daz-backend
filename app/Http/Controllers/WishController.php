<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wish;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class WishController extends Controller
{

    //GETTER
    public function getAllWishes()
    {
        return Wish::all();
    }

    public function getWishById($wishid)
    {
        try {
            $wish = Wish::select('wish_name')
                ->where('wish_id', $wishid)
                ->first();
            return $wish;
        } catch (\Exception $exception) {
            Log::error(sprintf("Failed to send the wish to our system!", $exception->getMessage()));
            return response()->json([
                "message" => "An unexpected error occured",
            ], 500);
        }
    }

    public function getWishesWithState($state)
    {
        try {
            $wish = Wish::select('wish_id', 'wish_name', 'review_state')
                ->where('review_state', $state)
                ->get();
            if ($wish == true ^ false) {
                return $wish;
            } else {
                return response()->json([
                    "not possible entry"
                ], 400);
            }
        } catch (\Exception $exception) {
            Log::error(sprintf("Failed to send the wish to our system!", $exception->getMessage()));
            return response()->json([
                "message" => "An unexpected error occured",
            ], 500);
        }
    }

    public function getWishesWithStateNull()
    {
        $wish = Wish::select('wish_id', 'wish_name', 'review_state')
            ->where('review_state', null)
            ->get();
        return $wish;
    }

    //SETTER
    public function createNewWish(Request $request)
    {
        try {
            $wish = new Wish;
            $wish->wish_name = $request->wish_name;
            $wish->save();
            if (
                $wish == null
            ) {
                return response()->json([
                    "Wish entry can't be nothing"
                ], 400);
            }
        } catch (\Exception $exception) {
            Log::error(sprintf("Failed to send the wish to our system!", $exception->getMessage()));
            return response()->json([
                "message" => "An unexpected error occured",
            ], 500);
        }
    }

    public function updateStateById(Request $request, $wishid)
    {
        try {
            $newState = $request->review_state;
            $oldState = Wish::find($wishid);
            if (($newState <= 1 && $newState > -1) || $newState == "") {
                $oldState->update(['review_state' => $newState]);
            } else {
                return response()->json([
                    "message" => "Value must be a boolean (0,1 or NULL)",
                ], 500);
            }
        } catch (\Exception $exception) {
            Log::error(sprintf("Failed to send the wish to our system!", $exception->getMessage()));
            return response()->json([
                "message" => "An unexpected error occured",
            ], 500);
        }
    }
}

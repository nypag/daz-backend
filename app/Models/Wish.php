<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wish extends Model
{
    protected $table = 'wish';

    protected $fillable = [
        'wish_id',
        'wish_name',
        'review_state',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $primaryKey = 'wish_id';
}

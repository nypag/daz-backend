<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\http\Controllers\WishController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Routes that get wishes and review_state of wishes
Route::prefix('wishes')->group(function () {
    Route::get('', [WishController::class, 'getAllWishes']);
    Route::get('null', [WishController::class, 'getWishesWithStateNull']);
    Route::get('{state}', [WishController::class, 'getWishesWithState']);
});

//Route that get a wish by id
Route::get('/wish/{wishid}', [WishController::class, 'getWishById']);

//Route that set a new wish
Route::post('/new', [WishController::class, 'createNewWish']);

//Route that change review_state of a wish
Route::post('/wish/{wishid}/update', [WishController::class, 'updateStateById']);

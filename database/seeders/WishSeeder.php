<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Wish;

class WishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wishes = [
            ['wish_id' => 1, 'wish_name' => 'Weltfrieden', 'review_state' => null],
            ['wish_id' => 2, 'wish_name' => 'Hoffnung', 'review_state' => true],
            ['wish_id' => 3, 'wish_name' => 'Menschenrechte', 'review_state' => true],
            ['wish_id' => 4, 'wish_name' => 'Freude', 'review_state' => true],
            ['wish_id' => 5, 'wish_name' => 'Spass', 'review_state' => false],
            ['wish_id' => 6, 'wish_name' => 'Wissen', 'review_state' => null],
            ['wish_id' => 7, 'wish_name' => 'Sport', 'review_state' => true],
            ['wish_id' => 8, 'wish_name' => 'JoelK', 'review_state' => false],
            ['wish_id' => 9, 'wish_name' => 'fluchwort', 'review_state' => false],
            ['wish_id' => 10, 'wish_name' => 'Michakaleo', 'review_state' => null],
            ['wish_id' => 11, 'wish_name' => 'Trymax', 'review_state' => true],
        ];

        foreach ($wishes as $wish) {
            $newWish = new Wish;
            $newWish->wish_id = $wish['wish_id'];
            $newWish->wish_name = $wish['wish_name'];
            $newWish->review_state = $wish['review_state'];
            $newWish->save();
        }
    }
}

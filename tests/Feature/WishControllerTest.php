<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Wish;

class WishControllerTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->createWishes();
    }

    private function createWishes()
    {
        $wishes = [
            ['wish_id' => 1, 'wish_name' => 'Weltfrieden', 'review_state' => null],
            ['wish_id' => 2, 'wish_name' => 'Excobar', 'review_state' => null],
            ['wish_id' => 3, 'wish_name' => 'Pizza für alle', 'review_state' => true],
            ['wish_id' => 4, 'wish_name' => 'Lehrabschluss', 'review_state' => true],
            ['wish_id' => 5, 'wish_name' => 'Künstler werden', 'review_state' => false],
            ['wish_id' => 6, 'wish_name' => 'Schweiz EM', 'review_state' => null],
            ['wish_id' => 7, 'wish_name' => 'JoelK', 'review_state' => false],
            ['wish_id' => 8, 'wish_name' => 'Unendlich Zeit', 'review_state' => true],
            ['wish_id' => 9, 'wish_name' => 'Geld', 'review_state' => true],
            ['wish_id' => 10, 'wish_name' => 'Menschenrechte', 'review_state' => true],
        ];
        foreach ($wishes as $wish) {
            $newWish = new Wish();
            $newWish->wish_id = $wish['wish_id'];
            $newWish->wish_name = $wish['wish_name'];
            $newWish->review_state = $wish['review_state'];
            $newWish->save();
        }
    }

    public function test_GivenWishes_WhenUrlNotChange_ThenReturnAllWishes()
    {
        //GIVEN
        $expectedState = 200;
        $expectedResult = [
            ["wish_id" => 1, "wish_name" => "Weltfrieden", "review_state" => null],
            ["wish_id" => 2, "wish_name" => "Excobar", "review_state" => null],
            ["wish_id" => 3, "wish_name" => "Pizza für alle", "review_state" => 1],
            ["wish_id" => 4, "wish_name" => "Lehrabschluss", "review_state" => 1],
            ["wish_id" => 5, "wish_name" => "Künstler werden", "review_state" => 0],
            ["wish_id" => 6, "wish_name" => "Schweiz EM", "review_state" => null],
            ["wish_id" => 7, "wish_name" => "JoelK", "review_state" => 0],
            ["wish_id" => 8, "wish_name" => "Unendlich Zeit", "review_state" => 1],
            ["wish_id" => 9, "wish_name" => "Geld", "review_state" => 1],
            ["wish_id" => 10, "wish_name" => "Menschenrechte", "review_state" => 1],
        ];

        //WHEN
        $response = $this->get(
            'http://127.0.0.1:8000/api/wishes',
        );
        //THEN
        $response->assertStatus($expectedState);
        $this->assertEquals(json_encode($expectedResult), json_encode($response->getData()));
    }

    public function test_GivenWishWithId_WhenUserMatchId_ThenReturnWishById()
    {
        //GIVEN
        $expectedState = 200;
        $expectedResult = [

            "wish_name" => "Weltfrieden",

        ];
        //WHEN
        $response = $this->get(
            'http://127.0.0.1:8000/api/wish/1',
        );
        //THEN
        $response->assertStatus($expectedState);
        $this->assertEquals(json_encode($expectedResult), json_encode($response->getData()));
    }

    public function test_GivenWishesWithReviewStateFalse_WhenWishesMatchWithUrl_ThenReturnMatchResults()
    {
        //GIVEN
        $expectedState = 200;
        $expectedResult = [
            ["wish_id" => 5, "wish_name" => "Künstler werden", "review_state" => 0],
            ["wish_id" => 7, "wish_name" => "JoelK", "review_state" => 0],

        ];
        //WHEN
        $response = $this->get(
            'http://127.0.0.1:8000/api/wishes/0',
        );
        //THEN
        $response->assertStatus($expectedState);
        $this->assertEquals(json_encode($expectedResult), json_encode($response->getData()));
    }

    public function test_GivenWishesWithReviewStateTrue_WhenWishesMatchWithUrl_ThenReturnMatchResults()
    {
        //GIVEN
        $expectedState = 200;
        $expectedResult = [
            ["wish_id" => 3, "wish_name" => "Pizza für alle", "review_state" => 1],
            ["wish_id" => 4, "wish_name" => "Lehrabschluss", "review_state" => 1],
            ["wish_id" => 8, "wish_name" => "Unendlich Zeit", "review_state" => 1],
            ["wish_id" => 9, "wish_name" => "Geld", "review_state" => 1],
            ["wish_id" => 10, "wish_name" => "Menschenrechte", "review_state" => 1],
        ];
        //WHEN
        $response = $this->get(
            'http://127.0.0.1:8000/api/wishes/1',
        );
        //THEN
        $response->assertStatus($expectedState);
        $this->assertEquals(json_encode($expectedResult), json_encode($response->getData()));
    }

    public function test_GivenWishesWithReviewStateNull_WhenWishesMatchWithUrl_ThenReturnMatchResults()
    {
        //GIVEN
        $expectedState = 200;
        $expectedResult = [
            ['wish_id' => 1, 'wish_name' => 'Weltfrieden', 'review_state' => null],
            ['wish_id' => 2, 'wish_name' => 'Excobar', 'review_state' => null],
            ['wish_id' => 6, 'wish_name' => 'Schweiz EM', 'review_state' => null],
        ];
        //WHEN
        $response = $this->get(
            'http://127.0.0.1:8000/api/wishes/null',
        );
        //THEN
        $response->assertStatus($expectedState);
        $this->assertEquals(json_encode($expectedResult), json_encode($response->getData()));
    }
}
